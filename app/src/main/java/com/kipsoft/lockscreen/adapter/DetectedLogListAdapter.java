package com.kipsoft.lockscreen.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.kipsoft.lockscreen.R;
import com.kipsoft.lockscreen.model.DetectedLog;

import java.text.DecimalFormat;
import java.util.List;

public class DetectedLogListAdapter extends RecyclerView.Adapter {

private Context mContext;
private List<DetectedLog> mDetectedLogList;

public DetectedLogListAdapter(Context context, List<DetectedLog> DetectedLogList) {
        mContext = context;
        mDetectedLogList = DetectedLogList;
        }

@Override
public int getItemCount() {
        return mDetectedLogList.size();
        }

// Determines the appropriate ViewType according to the sender of the DetectedLog.
@Override
public int getItemViewType(int position) {
        DetectedLog DetectedLog = (DetectedLog) mDetectedLogList.get(position);

        return position;
        }

// Inflates the appropriate layout according to the ViewType.
@Override
public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;


        view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.log_list_item, parent, false);
        return new DetectedLogHolder(view);


        }

// Passes the DetectedLog object to a ViewHolder so that the contents can be bound to UI.
@Override
public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DetectedLog DetectedLog = (DetectedLog) mDetectedLogList.get(position);


        ((DetectedLogHolder) holder).bind(DetectedLog);


        }

private class DetectedLogHolder extends RecyclerView.ViewHolder {
    TextView tv_date, tv_pin_entered , tv_sucess;

    DetectedLogHolder(View itemView) {
        super(itemView);


        tv_date = (TextView) itemView.findViewById(R.id.tv_date);
        tv_pin_entered = (TextView) itemView.findViewById(R.id.tv_pin_entered);
        tv_sucess = (TextView) itemView.findViewById(R.id.tv_success);

    }

    void bind(DetectedLog DetectedLog) {

        DecimalFormat formatter = new DecimalFormat("#,###,###");

        tv_date.setText(DetectedLog.getDate().toString().substring(0, 10));
        tv_pin_entered.setText(DetectedLog.getPin_entered().toString());
        tv_sucess.setText(DetectedLog.getSuccess().toString());

    }
}
}

