package com.kipsoft.lockscreen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Kiprop on 19/02/2017.
 */

public class PINActivity extends AppCompatActivity {
    PinLockView mPinLockView;
    String TAG="tag";
    IndicatorDots mIndicatorDots;
    MaterialDialog pDialog ,mDialog;
    Context context;
    String desc , current_pin , msisdn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_code_layout);


        context=PINActivity.this;

        //installCertificates();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PINActivity.this);

        current_pin = preferences.getString("current_pin", "");


        msisdn = preferences.getString("msisdn", "");

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mPinLockView.setPinLockListener(mPinLockListener);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        mPinLockView.attachIndicatorDots(mIndicatorDots);

        desc=getIntent().getStringExtra("desc");

    }



    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin_code) {
            Log.d(TAG, "Pin complete: " + pin_code);


            System.out.println("PIN entered is : "+pin_code );



            authenticatePIN(pin_code);
        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };


    private void authenticatePIN(String pin_code) {


if(pin_code.equals("1234")) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("result", desc);
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } else {

                                mDialog = new MaterialDialog.Builder(PINActivity.this)
                                        .title("Failed")
                                        .cancelable(false)
                                        .content("Wrong PIN !")
                                        .negativeText("Cancel")
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                mDialog.dismiss();
                                                Intent returnIntent = new Intent();
                                                setResult(Activity.RESULT_CANCELED, returnIntent);
                                                finish();

                                            }
                                        })
                                        .positiveText("Try Again")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                pDialog.dismiss();
                                                mDialog.dismiss();

                                                mPinLockView.resetPinLockView();

                                            }
                                        })
                                        .show();
                            }


    }
}
