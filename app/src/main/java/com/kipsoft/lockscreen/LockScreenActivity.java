package com.kipsoft.lockscreen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kipsoft.lockscreen.app.AppConfig;

import com.kipsoft.lockscreen.app.AppController;
import com.kipsoft.lockscreen.mail.SendMail;
import com.kipsoft.lockscreen.model.DetectedLog;
import com.kipsoft.lockscreen.utils.HomeKeyLocker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;



public class LockScreenActivity extends AppCompatActivity {

    PinLockView mPinLockView;
    String TAG="tag";
    IndicatorDots mIndicatorDots;
    MaterialDialog  mDialog;
    Context context;
    String desc , current_pin , msisdn, recipient_email, recipient_phone;
    Realm realm;
    HomeKeyLocker mHomeKeyLocker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_code_layout);


        context=LockScreenActivity.this;

        // Object of Class HomeKeyLocker.
       // mHomeKeyLocker = new HomeKeyLocker();
        setContentView(R.layout.pin_code_layout);
      //  getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        // Calling Function in Class HomeKeyLocker to Block Home Button on this Activity.
       // mHomeKeyLocker.lock(this);

        //installCertificates();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        current_pin = preferences.getString("pin", "");
        recipient_email = preferences.getString("email", "");
        recipient_phone = preferences.getString("phone", "");

        //Toast.makeText(context, current_pin , Toast.LENGTH_LONG).show();


        msisdn = preferences.getString("msisdn", "");

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mPinLockView.setPinLockListener(mPinLockListener);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        mPinLockView.attachIndicatorDots(mIndicatorDots);

        desc=getIntent().getStringExtra("desc");

        // Initialize Realm
        Realm.init(context);


        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();


    }



    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin_code) {
            Log.d(TAG, "Pin complete: " + pin_code);


            System.out.println("PIN entered is : "+pin_code );



            authenticatePIN(pin_code);
        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };


    private void authenticatePIN(final String pin_code) {

        final String success ;

sendEmail(recipient_email,new Date().toString());



        if(pin_code.equals(current_pin)) {
//            Intent returnIntent = new Intent();
//            returnIntent.putExtra("result", desc);
//            setResult(Activity.RESULT_OK, returnIntent);
            success = "SUCCESSFUL";
            finish();

        } else {
//send sms only on bad attempt
            sendSMS(recipient_phone,new Date().toString());


            success = "FAILED";

            mDialog = new MaterialDialog.Builder(context)
                    .title("Failed")
                    .cancelable(false)
                    .content("Wrong PIN ! \nThis attempt has been logged")
                    .positiveText("Try Again")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            mDialog.dismiss();

                            mPinLockView.resetPinLockView();

                        }
                    })
                    .show();

        }

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    DetectedLog log =  new DetectedLog();
                    log.setDate(new Date().toString());
                    log.setPin_entered(pin_code);
                    log.setSuccess(success);
                    log.setId(new Date().toString());

                    realm.copyToRealmOrUpdate(log);

                    // realm.commitTransaction();
                }
            });
            




    }

    private void sendEmail(String recipient_email,String time) {
        //Getting content for email

        System.out.println("emeail set is ="+recipient_email+"=");
        String email = recipient_email;
        String subject = "AIDS";
        String message = "Intrusion has been detected at "+ time;

        //Creating SendMail object
        SendMail sm = new SendMail(this, email, subject, message);

        //Executing sendmail to send email
        sm.execute();
    }

    private void sendSMS(String recipient_phone,String time) {


        String phone = recipient_phone;


        String url = AppConfig.SMS_URL + phone + "&message=" + "Intrusion_detected_at_" + time.replaceAll(" ","_");


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });




//add request to volley queue
       AppController.getInstance().
                addToRequestQueue(
                        request, "sms_request");




    }
    @Override
    protected void onUserLeaveHint()
    {
        Log.d("onUserLeaveHint","Home button pressed");
        //super.onUserLeaveHint();
    }


    @Override
    public void onAttachedToWindow() {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
//                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
       // this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        super.onAttachedToWindow();

    }

//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//        if (hasFocus) {
//            getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//        }
//    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        System.out.println("KEYCODE: " + keyCode + " "+ event);
        if ((keyCode == KeyEvent.KEYCODE_HOME)) {
            System.out.println("KEYCODE_HOME");
            return true;
        }
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            System.out.println("KEYCODE_BACK");
            return true;
        }
        if ((keyCode == KeyEvent.KEYCODE_MENU)) {
            System.out.println("KEYCODE_MENU");
            return true;
        }
        return false;
    }




}
