package com.kipsoft.lockscreen.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kipsoft.lockscreen.MainActivity;
import com.kipsoft.lockscreen.R;
import com.kipsoft.lockscreen.utils.LockScreen;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Kiprop on 14/02/2017.
 */


public class Home extends Fragment {



    Context context;
    AppCompatButton btn_apply_now;
    String email, phone;
    @BindView(R.id.btnEdit)
    Button btnEdit;

    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.toggleButton)
    ToggleButton toggleButton;

    Button bEdit;

    MaterialDialog materialDialog;
    TextView tv_loan_balance , tv_due_on;
    AppCompatButton btn_pay , btn_proceed ,btn_cancel;
    AppCompatEditText ed_phone, ed_email , ed_pin;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View x = inflater.inflate(R.layout.home, null);

context = getActivity();


        return x;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

ButterKnife.bind(this , getView());
        context = getActivity();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        email = preferences.getString("email", "");
        phone = preferences.getString("phone", "");

        tvEmail.setText(" Email: "+email);
        tvPhone.setText(" Phone: "+phone);



        LockScreen.getInstance().init(getActivity(),true);
        if(LockScreen.getInstance().isActive()){
            toggleButton.setChecked(true);
        }else{
            toggleButton.setChecked(false);

        }


        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if(checked){

                    LockScreen.getInstance().active();
                }else{
                    LockScreen.getInstance().deactivate();
                }
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                materialDialog = new MaterialDialog.Builder(getActivity())


                        .customView(R.layout.prompt_enter_details,true)
                        .show();

                View promptsView = materialDialog.getCustomView();

                ed_email=(AppCompatEditText) promptsView.findViewById(R.id.ed_email);
                ed_pin=(AppCompatEditText) promptsView.findViewById(R.id.ed_pin);
                ed_phone=(AppCompatEditText) promptsView.findViewById(R.id.ed_phone);

                ed_email.setHint(email);
                ed_phone.setHint(phone);

               String email = ed_email.getText().toString();
               String  phone = ed_phone.getText().toString();

                btn_proceed= (AppCompatButton) promptsView.findViewById(R.id.btn_proceed);

                btn_proceed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("email",ed_email.getText().toString());
                        editor.putString("phone", ed_phone.getText().toString());
                        editor.putString("pin", ed_pin.getText().toString());


                        editor.apply();

                        tvEmail.setText(ed_email.getText().toString());
                        tvPhone.setText(ed_phone.getText().toString());

                        materialDialog.dismiss();

                    }
                });






            }
        });



    }}
