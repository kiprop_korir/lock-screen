package com.kipsoft.lockscreen.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.kipsoft.lockscreen.R;
import com.kipsoft.lockscreen.adapter.DetectedLogListAdapter;
import com.kipsoft.lockscreen.model.DetectedLog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;


/**
 * Created by Kiprop on 14/02/2017.
 */


public class Logs extends Fragment {



    Context context;
    AppCompatButton btn_apply_now;
    Realm realm;
    RealmResults<DetectedLog> realmDetectedLogs;

    @BindView(R.id.rvLogs)
    RecyclerView rvDetectedLogs;

    @BindView(R.id.btnDeleteLogs)
    Button btnDeleteLogs;

    private DetectedLogListAdapter logListAdapter;
    private ArrayList<DetectedLog> logListAdapterArrayList = new ArrayList<DetectedLog>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View z = inflater.inflate(R.layout.logs, null);


        return z;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Initialize Realm
        Realm.init(getActivity());
        
        ButterKnife.bind(this, getView());


        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();

        context = getActivity();

        //get all screenData from realm and store in ascending order


        realmDetectedLogs = realm.where(DetectedLog.class).findAll();

        btnDeleteLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Initialize Realm
                Realm.init(context);
                // Get a Realm instance for this thread
                realm = Realm.getDefaultInstance();

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realmDetectedLogs.deleteAllFromRealm();
                        rvDetectedLogs.notify();

                        Toast.makeText(context, "Deleted", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

        for(int x = 0; x<realmDetectedLogs.size() ; x++){

            DetectedLog log = new DetectedLog();


            log.setDate(realmDetectedLogs.get(x).getDate());
            log.setPin_entered(realmDetectedLogs.get(x).getPin_entered());
            log.setSuccess(realmDetectedLogs.get(x).getSuccess());
           

            logListAdapterArrayList.add(log);
            
        }

        logListAdapter = new DetectedLogListAdapter(getActivity(), logListAdapterArrayList);

        rvDetectedLogs.setLayoutManager(new LinearLayoutManager(getActivity()));
        logListAdapter.notifyDataSetChanged();
        rvDetectedLogs.setAdapter(logListAdapter);


    }



}
