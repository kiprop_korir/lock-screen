package com.kipsoft.lockscreen.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DetectedLog extends RealmObject {

    @PrimaryKey
    private String id;

    private String date , pin_entered , success;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPin_entered() {
        return pin_entered;
    }

    public void setPin_entered(String pin_entered) {
        this.pin_entered = pin_entered;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
