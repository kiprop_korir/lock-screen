package com.kipsoft.lockscreen;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kipsoft.lockscreen.fragments.Home;
import com.kipsoft.lockscreen.fragments.Logs;
import com.kipsoft.lockscreen.fragments.ResourceUsage;
import com.kipsoft.lockscreen.utils.LockScreen;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialize.color.Material;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    ToggleButton toggleButton;
    Toolbar toolbar;
    Context context;
    private Drawer result = null;

    final  int PERMISSION_ALL = 1;

    String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);


        context = MainActivity.this;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

        }


        ButterKnife.bind(this);



        // Handle Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>AIDS</font>"));

        createDrawer(savedInstanceState);





        //set default home page

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment fragment = new Fragment();
        fragment = new Home();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();




    }



private void createDrawer(Bundle bundle){
    //Create the drawer


    result = new DrawerBuilder()
            .withActivity(this)
            .withToolbar(toolbar)
            .withDrawerWidthDp(250)
            .withAccountHeader(new AccountHeaderBuilder()
                    .withActivity(this)
                    .withHeaderBackground(R.color.md_red_300)
                    .withCompactStyle(false)
                    .addProfiles(
                             new ProfileDrawerItem().withName("AIDS").withEmail("Android Intrusion Detection System"))
                    .withSelectionListEnabledForSingleProfile(false)
                    .withSavedInstance(bundle)
                    .build())
            .withSliderBackgroundColor(getResources().getColor(R.color.white))
            .addDrawerItems(
                    new PrimaryDrawerItem().withName("Home").withIconTintingEnabled(true).withIcon(R.mipmap.ic_home).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                            System.out.println("Home");
                            FragmentManager manager = getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            Fragment fragment = new Fragment();
                            getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Home</font>"));
                            fragment = new Home();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.commit();
                            return false;
                        }
                    }),

                    new PrimaryDrawerItem().withName("Logs").withIconTintingEnabled(true).withIcon(R.mipmap.ic_logs).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                            FragmentManager manager = getSupportFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            Fragment fragment = new Fragment();

                            getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Logs</font>"));
                            fragment = new Logs();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.commit();
                            return false;
                        }
                    })
                    )

                .withSavedInstance(bundle)
            .build();
}
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                }else{

                        // Permission denied, Disable the functionality that depends on getActivity() permission.
                        Toast.makeText(MainActivity.this, "permission denied", Toast.LENGTH_LONG).show();
                    }

                }
                return;
            }


    }



    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
